// Get the elements
var link = document.getElementById("link");
var modal = document.getElementById("modal");
var modalContent = document.getElementById("modal-content");
var closeButton = document.getElementById("close-button");

// Open the modal when the link is clicked
link.onclick = function(event) {
  // Prevent the default action of opening the link
  event.preventDefault();
  // Get the href attribute of the link
  var href = this.getAttribute("href");
  // Set the src attribute of the iframe to the href
  modalContent.src = href;
  // Display the modal
  modal.style.display = "block";
}

// Close the modal when the user clicks outside the modal on the close button
window.onclick = function(event) {
  if (event.target == modal || event.target == closeButton) {
    // Hide the modal
    modal.style.display = "none";
    // Clear the src attribute of the iframe
    modalContent.src = "";
  }
}

